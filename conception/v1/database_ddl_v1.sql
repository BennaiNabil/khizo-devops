DROP DATABASE IF EXISTS khizo;
CREATE DATABASE IF NOT EXISTS khizo;

DROP USER IF EXISTS 'khizouser'@'localhost';

CREATE USER 'khizouser'@'localhost' IDENTIFIED BY 'a1ded7a09992ff9370a62fcef4987601';
GRANT SELECT, INSERT, UPDATE, DELETE ON khizouser.* TO 'khizouser'@'localhost';

USE khizo;

CREATE TABLE users
(
    iduser   INT PRIMARY KEY AUTO_INCREMENT AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL UNIQUE,
    email    VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    bandate  DATETIME
);

CREATE TABLE recipes
(
    idrecipe      INT PRIMARY KEY AUTO_INCREMENT,
    iduser        INT          NOT NULL,
    title         VARCHAR(255) NOT NULL,
    isprivate     BOOLEAN      NOT NULL,
    lastvisitdate DATE,
    FOREIGN KEY (iduser) REFERENCES users (iduser)
);

CREATE TABLE steps
(
    idstep      INT PRIMARY KEY AUTO_INCREMENT,
    idrecipe    INT  NOT NULL,
    description TEXT NOT NULL,
    content     TEXT,
    FOREIGN KEY (idrecipe) REFERENCES recipes (idrecipe)
);

CREATE TABLE ingredients
(
    idingredient INT PRIMARY KEY AUTO_INCREMENT,
    name         VARCHAR(255) NOT NULL
);

CREATE TABLE recipeingredients
(
    idrecipe     INT          NOT NULL,
    idingredient INT          NOT NULL,
    quantity     VARCHAR(255) NOT NULL,
    PRIMARY KEY (idrecipe, idingredient),
    FOREIGN KEY (idrecipe) REFERENCES recipes (idrecipe),
    FOREIGN KEY (idingredient) REFERENCES ingredients (idingredient)
);

CREATE TABLE friendships
(
    iduser1 INT NOT NULL,
    iduser2 INT NOT NULL,
    PRIMARY KEY (iduser1, iduser2),
    FOREIGN KEY (iduser1) REFERENCES users (iduser),
    FOREIGN KEY (iduser2) REFERENCES users (iduser)
);

CREATE TABLE ratings
(
    idrating INT PRIMARY KEY AUTO_INCREMENT,
    iduser   INT NOT NULL,
    idrecipe INT NOT NULL,
    value    INT CHECK (value BETWEEN 1 AND 5),
    FOREIGN KEY (iduser) REFERENCES users (iduser),
    FOREIGN KEY (idrecipe) REFERENCES recipes (idrecipe)
);

CREATE TABLE roles
(
    idrole   INT PRIMARY KEY AUTO_INCREMENT,
    rolename VARCHAR(255) NOT NULL
);

CREATE TABLE userroles
(
    iduser INT NOT NULL,
    idrole INT NOT NULL,
    PRIMARY KEY (iduser, idrole),
    FOREIGN KEY (iduser) REFERENCES users (iduser),
    FOREIGN KEY (idrole) REFERENCES roles (idrole)
);

CREATE TABLE userhasfavorites
(
    iduser     INT NOT NULL,
    idfavorite INT NOT NULL,
    primary key (iduser, idfavorite),
    FOREIGN KEY (iduser) REFERENCES users (iduser),
    FOREIGN KEY (idfavorite) REFERENCES recipes (idrecipe)
);

CREATE TABLE parameters
(
    `key` VARCHAR(255) PRIMARY KEY,
    value VARCHAR(64) NOT NULL
);

CREATE TABLE notifications
(
    idnotification INT PRIMARY KEY AUTO_INCREMENT AUTO_INCREMENT,
    iduser         INT  NOT NULL,
    content        TEXT NOT NULL,
    createdat      DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (iduser) REFERENCES users (iduser)
);


INSERT INTO parameters (`key`, value)
VALUES ('MAX_NB_FRIENDS', '32');


DELIMITER //
CREATE PROCEDURE create_notification_procedure(user_id INT)
BEGIN
    INSERT INTO notifications (iduser, content)
    VALUES (user_id, 'You have received a new five star rating!');
END;
//
DELIMITER ;


DELIMITER //

CREATE TRIGGER notification_on_rating_trigger
    BEFORE INSERT
    ON ratings
    FOR EACH ROW
BEGIN
    IF NEW.value = 5 THEN
        CALL create_notification_procedure(NEW.iduser);
    END IF;
END;
//

DELIMITER ;

CREATE INDEX idx_email_user ON users (email);

insert into roles (idrole, rolename)
values (1, 'ADMIN'),
       (2, 'USER');

insert into users (email, password, bandate, username)
values ('mail@gmail.com', 'mdpasse', '2019-01-01', 'username');

insert into userroles (iduser, idrole)
values (1, 2),
       (1, 1);

insert into recipes (idrecipe, iduser, title, isprivate, lastvisitdate)
values (1, 1, 'Ex pariatur esse', false, null);

insert into ratings (idrating, iduser, idrecipe, value)
values (1, 1, 1, 5);