DROP DATABASE IF EXISTS khizo2;
CREATE DATABASE IF NOT EXISTS khizo2;

DROP USER IF EXISTS 'khizouser'@'localhost';

CREATE USER 'khizouser'@'localhost' IDENTIFIED BY 'a1ded7a09992ff9370a62fcef4987601';
GRANT SELECT, INSERT, UPDATE, DELETE ON khizouser.* TO 'khizouser'@'localhost';

USE khizo2;

SET foreign_key_checks = 0;

CREATE TABLE khizo2.parameters
(
    `key` VARCHAR(255) NOT NULL
        PRIMARY KEY,
    value INT          NOT NULL
);

CREATE TABLE khizo2.roles
(
    idrole   BIGINT AUTO_INCREMENT
        PRIMARY KEY,
    rolename ENUM ('ADMIN', 'USER') NULL
);

CREATE TABLE khizo2.users
(
    iduser       BIGINT AUTO_INCREMENT
        PRIMARY KEY,
    email        VARCHAR(255) NULL,
    password     VARCHAR(255) NULL,
    username     VARCHAR(255) NULL,
    banned_at    DATETIME(6)  NULL,
    banned_until DATETIME(6)  NULL,
    CONSTRAINT UK6dotkott2kjsp8vw4d0m25fb7
        UNIQUE (email),
    CONSTRAINT UKr43af9ap4edm43mmtq01oddj6
        UNIQUE (username)
);

CREATE TABLE khizo2.notifications
(
    idnotification BIGINT AUTO_INCREMENT
        PRIMARY KEY,
    content        VARCHAR(255)                        NULL,
    created_at     TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    iduser         BIGINT                              NULL,
    CONSTRAINT FKe9vmdulcq55er680ntuluxbfb
        FOREIGN KEY (iduser) REFERENCES khizo2.users (iduser)
);

CREATE TABLE khizo2.recipes
(
    idrecipe       BIGINT AUTO_INCREMENT
        PRIMARY KEY,
    created_at     DATETIME(6)  NULL,
    description    VARCHAR(255) NULL,
    is_private     BIT          NULL,
    last_viewed_at DATETIME(6)  NULL,
    title          VARCHAR(255) NULL,
    idcreator      BIGINT       NULL,
    CONSTRAINT FKi4iq72d1ovgcsddkjqtep08qa
        FOREIGN KEY (idcreator) REFERENCES khizo2.users (iduser)
);

CREATE TABLE khizo2.ingredients
(
    idingredient BIGINT AUTO_INCREMENT
        PRIMARY KEY,
    name         VARCHAR(255) NULL,
    quantity     DOUBLE       NULL,
    unit         VARCHAR(255) NULL,
    idrecipe     BIGINT       NULL,
    CONSTRAINT FKnvrk8ksa7frwnufn8qlayq01m
        FOREIGN KEY (idrecipe) REFERENCES khizo2.recipes (idrecipe)
);

CREATE TABLE khizo2.ratings
(
    idrating BIGINT AUTO_INCREMENT
        PRIMARY KEY,
    value    INT    NULL,
    idrecipe BIGINT NULL,
    iduser   BIGINT NULL,
    CONSTRAINT FK3s4edvg11k09apic55ysuypa5
        FOREIGN KEY (iduser) REFERENCES khizo2.users (iduser),
    CONSTRAINT FK772qn3ppqe9wos5tdatc4svig
        FOREIGN KEY (idrecipe) REFERENCES khizo2.recipes (idrecipe)
);

CREATE DEFINER = root@`%` TRIGGER khizo2.notification_on_rating_trigger
    BEFORE INSERT
    ON khizo2.ratings
    FOR EACH ROW
BEGIN
    IF NEW.value = 5 THEN
        CALL create_notification_procedure(NEW.iduser);
    END IF;
END;

CREATE TABLE khizo2.steps
(
    idstep      BIGINT AUTO_INCREMENT
        PRIMARY KEY,
    content     VARCHAR(255) NULL,
    description VARCHAR(255) NULL,
    step_order  INT          NULL,
    idrecipe    BIGINT       NULL,
    CONSTRAINT FK6j11ht2emmp0v34fswbd2kc6s
        FOREIGN KEY (idrecipe) REFERENCES khizo2.recipes (idrecipe)
);

CREATE TABLE khizo2.user_favorites
(
    iduser   BIGINT NOT NULL,
    idrecipe BIGINT NOT NULL,
    CONSTRAINT FK68d6y74alixg33bo8k5kjl9iu
        FOREIGN KEY (idrecipe) REFERENCES khizo2.recipes (idrecipe),
    CONSTRAINT FKhfs1w7moyul7eudyex43q5yj9
        FOREIGN KEY (iduser) REFERENCES khizo2.users (iduser)
);

CREATE TABLE khizo2.user_friends
(
    iduser   BIGINT NOT NULL,
    idfriend BIGINT NOT NULL,
    PRIMARY KEY (iduser, idfriend),
    CONSTRAINT FK1fgaes03mj0iqwhq7q7l2kusw
        FOREIGN KEY (iduser) REFERENCES khizo2.users (iduser),
    CONSTRAINT FKjelyvstgdxx27hxgt3uulpkex
        FOREIGN KEY (idfriend) REFERENCES khizo2.users (iduser)
);

CREATE TABLE khizo2.user_role
(
    iduser BIGINT NOT NULL,
    idrole BIGINT NOT NULL,
    PRIMARY KEY (iduser, idrole),
    CONSTRAINT FKei2m2yvjthr3e00f7n66fgmqw
        FOREIGN KEY (iduser) REFERENCES khizo2.users (iduser),
    CONSTRAINT FKt67ybnqi8phbny11vosasys3u
        FOREIGN KEY (idrole) REFERENCES khizo2.roles (idrole)
);


CREATE INDEX idx_email_user
    ON users (email);

DELIMITER //
CREATE PROCEDURE create_notification_procedure(user_id INT)
BEGIN
    INSERT INTO notifications (iduser, content)
    VALUES (user_id, 'You have received a new five star rating!');
END;
//
DELIMITER ;


DROP TRIGGER IF EXISTS notification_on_rating_trigger;

DELIMITER //
CREATE TRIGGER notification_on_rating_trigger
    BEFORE INSERT
    ON ratings
    FOR EACH ROW
BEGIN
    IF NEW.value = 5 THEN
        CALL create_notification_procedure(NEW.iduser);
    END IF;
END;
//

DELIMITER ;

SET foreign_key_checks = 1;