package org.simplon.khizo.services;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.simplon.khizo.models.*;
import org.simplon.khizo.models.dto.HomepageDataDto;
import org.simplon.khizo.repositories.RatingEntityRepository;
import org.simplon.khizo.repositories.RecipeEntityRepository;
import org.simplon.khizo.repositories.RoleEntityRepository;
import org.simplon.khizo.repositories.UserEntityRepository;
import org.simplon.khizo.security.dto.RegisterDto;
import org.simplon.khizo.security.service.AppUserSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
class RecipeServiceTest {
    @Autowired
    private RatingEntityRepository ratingEntityRepository;
    @Autowired
    private RecipeEntityRepository recipeEntityRepository;
    @Autowired
    private RoleEntityRepository roleEntityRepository;
    @Autowired
    private UserEntityRepository userEntityRepository;
    @Autowired
    private AppUserSecurityService appUserSecurityService;
    @Autowired
    private RecipeService recipeService;

    private UserEntity user1;
    private RecipeEntity recipe1;

    @BeforeEach
    void setUpTest() {
        log.info("----------------------------------------------------------");
        log.info("Before each test");
        RoleEntity roleUser = new RoleEntity();
        roleUser.setName(RoleName.USER);
        roleUser = roleEntityRepository.save(roleUser);

        RoleEntity roleAdmin = new RoleEntity();
        roleAdmin.setName(RoleName.ADMIN);
        roleAdmin = roleEntityRepository.save(roleAdmin);

        RegisterDto user = new RegisterDto();
        user.setUsername("user");
        user.setEmail("user@gmail.com");
        user.setPassword("user");
        appUserSecurityService.register(user, RoleName.USER);

        this.user1 = userEntityRepository.findByUsernameOrEmail("user", "user")
                .orElseThrow(() -> new RuntimeException("User not found"));

        this.recipe1 = new RecipeEntity();
        this.recipe1.setTitle("recipe");
        this.recipe1.setDescription("recipe");
        this.recipe1.setIsPrivate(false);
        this.recipe1.setCreator(this.user1);
        RatingEntity rating1 = new RatingEntity(1);
        rating1.setIduser(this.user1);
        rating1.setIdrecipe(this.recipe1);
        RatingEntity rating2 = new RatingEntity(2);
        rating2.setIduser(this.user1);
        rating2.setIdrecipe(this.recipe1);
        RatingEntity rating3 = new RatingEntity(3);
        rating3.setIduser(this.user1);
        rating3.setIdrecipe(this.recipe1);
        List<RatingEntity> ratings = List.of(rating1, rating2, rating3);
        this.recipe1.setRatings(ratings);
        recipeEntityRepository.save(this.recipe1);
    }


    @AfterEach
    void clearTest() {
        log.info("----------------------------------------------------------");
        log.info("Clear test");
        recipeEntityRepository.deleteAll();
        userEntityRepository.deleteAll();
        roleEntityRepository.deleteAll();
    }

    @Test
    public void testCalculateAverageRating_WithEmptyList_ShouldReturnNull() {
        log.info("----------------------------------------------------------");
        log.info("Test calculate average rating with empty list");
        List<RatingEntity> emptyList = new ArrayList<>();
        Double result = recipeService.calculateAverageRating(emptyList);
        assertNull(result);
    }

    @Test
    public void testCalculateAverageRating_WithSingleRating_ShouldReturnRatingValue() {
        log.info("----------------------------------------------------------");
        log.info("Test calculate average rating with single rating");
        List<RatingEntity> ratings = new ArrayList<>();
        ratings.add(new RatingEntity(4));
        Double result = recipeService.calculateAverageRating(ratings);
        assertEquals(4, result);
    }

    @Test
    public void testCalculateAverageRating_WithMultipleRatings_ShouldReturnCorrectAverage() {
        log.info("----------------------------------------------------------");
        log.info("Test calculate average rating with multiple ratings");
        List<RatingEntity> ratings = new ArrayList<>();
        ratings.add(new RatingEntity(4));
        ratings.add(new RatingEntity(3));
        ratings.add(new RatingEntity(2));

        Double result = recipeService.calculateAverageRating(ratings);
        assertEquals(3, result);
    }

    @Test
    void createRecipe() {
        log.info("----------------------------------------------------------");
        log.info("Create recipe service call");
        recipeService.createRecipe(this.recipe1, "user");
        assertNotNull(this.recipe1.getIdRecipe());
        assertNotEquals(this.recipe1.getIdRecipe(), 0);
    }

    @Test
    void deleteRecipe() {
        log.info("----------------------------------------------------------");
        log.info("Delete recipe service call");
        recipeService.deleteRecipe(this.recipe1.getIdRecipe());
        assertFalse(recipeEntityRepository.existsById(this.recipe1.getIdRecipe()));
    }

    @Test
    void getRecipesAvailable() {
        log.info("----------------------------------------------------------");
        log.info("Get recipes available service call");
        List<RecipeEntity> recipes = recipeService.getRecipesAvailable("user");
        assertEquals(recipes.size(), 1);
    }

    @Test
    void getRecipeById() {
        log.info("----------------------------------------------------------");
        log.info("Get recipe by id service call");
        RecipeEntity recipe = recipeService.getRecipeById(this.recipe1.getIdRecipe());
        assertEquals(recipe.getTitle(), "recipe");
    }

    @Test
    void getRecipeByIdMute() {
        log.info("----------------------------------------------------------");
        log.info("Get recipe by id mute service call");
        RecipeEntity recipe = recipeService.getRecipeByIdMute(this.recipe1.getIdRecipe());
        assertEquals(recipe.getTitle(), "recipe");
    }

    @Test
    void getHomepageData() {
        log.info("----------------------------------------------------------");
        log.info("Get homepage data service call");
        HomepageDataDto homepageDataDto = recipeService.getHomepageData("user");
        assertEquals(homepageDataDto.getNbRecipes(), 1);
    }

    @Test
    void recordVisit() {
        log.info("----------------------------------------------------------");
        log.info("Record visit service call");
        RecipeEntity recipe = recipeService.recordVisit(this.recipe1);
        assertNotNull(recipe.getLastViewedAt());

    }


}
