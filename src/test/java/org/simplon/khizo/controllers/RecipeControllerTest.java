package org.simplon.khizo.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.simplon.khizo.models.RecipeEntity;
import org.simplon.khizo.models.UserEntity;
import org.simplon.khizo.models.dto.HomepageDataDto;
import org.simplon.khizo.services.RatingService;
import org.simplon.khizo.services.RecipeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class RecipeControllerTest {

    @InjectMocks
    private RecipeController recipeController;

    @Mock
    private RecipeService recipeService;

    @Mock
    private RatingService ratingService;

    @Mock
    private Authentication authentication;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllRecipesOfUser() {
        List<RecipeEntity> recipes = new ArrayList<>();
        when(authentication.getName()).thenReturn("testUser");
        when(recipeService.getRecipesAvailable("testUser")).thenReturn(recipes);

        ResponseEntity<List<RecipeEntity>> response = recipeController.getAllRecipesOfUser(authentication);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(recipes, response.getBody());
    }

    @Test
    public void testGetRecipeById() {
        Long recipeId = 1L;
        RecipeEntity recipe = new RecipeEntity();
        UserEntity user = new UserEntity();
        user.setUsername("testUser");
        recipe.setCreator(user);
        when(authentication.getName()).thenReturn("testUser");
        when(recipeService.getRecipeById(recipeId)).thenReturn(recipe);
        when(recipeService.checkOwner(recipe, "testUser", "testUser")).thenReturn(true);


        ResponseEntity<RecipeEntity> response = recipeController.getRecipeById(recipeId, authentication);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(recipe, response.getBody());
    }

    @Test
    public void testGetRecipeByIdUnauthorized() {
        Long recipeId = 1L;
        RecipeEntity recipe = new RecipeEntity();
        UserEntity user = new UserEntity();
        user.setUsername("testUser");
        recipe.setCreator(user);
        when(authentication.getName()).thenReturn("testUser");
        when(recipeService.getRecipeById(recipeId)).thenReturn(recipe);
        when(recipeService.checkOwner(recipe, "testUser", "testUser")).thenReturn(false);


        ResponseEntity<RecipeEntity> response = recipeController.getRecipeById(recipeId, authentication);

        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }

    @Test
    public void testCreateRecipe() {
        RecipeEntity recipe = new RecipeEntity();
        UserEntity user = new UserEntity();
        user.setUsername("testUser");
        recipe.setCreator(user);
        when(authentication.getName()).thenReturn("testUser");
        when(recipeService.createRecipe(recipe, "testUser")).thenReturn(recipe);

        ResponseEntity<RecipeEntity> response = recipeController.createRecipe(recipe, authentication);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(recipe, response.getBody());
    }

    @Test
    public void testDeleteRecipe() {
        RecipeEntity recipe = new RecipeEntity();
        UserEntity user = new UserEntity();
        user.setUsername("testUser");
        recipe.setCreator(user);
        when(authentication.getName()).thenReturn("testUser");
        when(recipeService.getRecipeByIdMute(1L)).thenReturn(recipe);
        when(recipeService.checkOwner(recipe, "testUser", "testUser")).thenReturn(true);

        ResponseEntity<Void> response = recipeController.deleteRecipe(1L, authentication);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void testDeleteRecipeUnauthorized() {
        RecipeEntity recipe = new RecipeEntity();
        UserEntity user = new UserEntity();
        user.setUsername("testUser");
        recipe.setCreator(user);
        when(authentication.getName()).thenReturn("testUser");
        when(recipeService.getRecipeByIdMute(1L)).thenReturn(recipe);
        when(recipeService.checkOwner(recipe, "testUser", "testUser")).thenReturn(false);

        ResponseEntity<Void> response = recipeController.deleteRecipe(1L, authentication);

        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }

    @Test
    public void testFavorite() {
        RecipeEntity recipe = new RecipeEntity();
        UserEntity user = new UserEntity();
        user.setUsername("testUser");
        recipe.setCreator(user);
        when(authentication.getName()).thenReturn("testUser");
        when(recipeService.getRecipeByIdMute(1L)).thenReturn(recipe);
        when(recipeService.checkOwner(recipe, "testUser", "testUser")).thenReturn(true);

        ResponseEntity<Void> response = recipeController.favoriteRecipe(1L, authentication);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void testFavoriteUnauthorized() {
        RecipeEntity recipe = new RecipeEntity();
        UserEntity user = new UserEntity();
        user.setUsername("testUser");
        recipe.setCreator(user);
        when(authentication.getName()).thenReturn("testUser");
        when(recipeService.getRecipeByIdMute(1L)).thenReturn(recipe);
        when(recipeService.checkOwner(recipe, "testUser", "testUser")).thenReturn(false);

        ResponseEntity<Void> response = recipeController.favoriteRecipe(1L, authentication);

        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }

    @Test
    public void testUnfavorite() {
        RecipeEntity recipe = new RecipeEntity();
        UserEntity user = new UserEntity();
        user.setUsername("testUser");
        recipe.setCreator(user);
        when(authentication.getName()).thenReturn("testUser");
        when(recipeService.getRecipeByIdMute(1L)).thenReturn(recipe);
        when(recipeService.checkOwner(recipe, "testUser", "testUser")).thenReturn(true);
        when(recipeService.isRecipeFavorited(1L, "testUser")).thenReturn(true);

        ResponseEntity<Void> response = recipeController.unfavoriteRecipe(1L, authentication);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void testUnfavoriteUnauthorized() {
        RecipeEntity recipe = new RecipeEntity();
        UserEntity user = new UserEntity();
        user.setUsername("testUser");
        recipe.setCreator(user);
        when(authentication.getName()).thenReturn("testUser");
        when(recipeService.getRecipeByIdMute(1L)).thenReturn(recipe);
        when(recipeService.checkOwner(recipe, "testUser", "testUser")).thenReturn(false);

        ResponseEntity<Void> response = recipeController.unfavoriteRecipe(1L, authentication);

        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }

    @Test
    public void testRate() {
        ResponseEntity<Void> response = recipeController.rateRecipe(null, authentication);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void testGetHomepageData() {
        ResponseEntity<HomepageDataDto> response = recipeController.getHomepageData(authentication);

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }


}
