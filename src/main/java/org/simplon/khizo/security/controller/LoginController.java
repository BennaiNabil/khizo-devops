package org.simplon.khizo.security.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.simplon.khizo.models.RoleName;
import org.simplon.khizo.security.dto.BearerToken;
import org.simplon.khizo.security.dto.LoginDto;
import org.simplon.khizo.security.dto.RegisterDto;
import org.simplon.khizo.security.service.IAppUserSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/auth")
@Tag(name = "Login Controller", description = "API pour les opérations d'authentification")
public class LoginController {

    @Autowired
    private IAppUserSecurityService userSecurityService;

    @PostMapping("/signin")
    public BearerToken authenticateUser(@RequestBody LoginDto loginDto) {
        return userSecurityService.authenticate(loginDto);
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@RequestBody RegisterDto registerDto) {
        return userSecurityService.register(registerDto, RoleName.USER);
    }
}
