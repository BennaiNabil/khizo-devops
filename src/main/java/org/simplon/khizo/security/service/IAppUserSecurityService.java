package org.simplon.khizo.security.service;


import org.simplon.khizo.models.RoleName;
import org.simplon.khizo.security.dto.BearerToken;
import org.simplon.khizo.security.dto.LoginDto;
import org.simplon.khizo.security.dto.RegisterDto;
import org.springframework.http.ResponseEntity;

public interface IAppUserSecurityService {

    ResponseEntity<?> register(RegisterDto rRegisterDto, RoleName roleName);

    BearerToken authenticate(LoginDto rLoginDto);
}
