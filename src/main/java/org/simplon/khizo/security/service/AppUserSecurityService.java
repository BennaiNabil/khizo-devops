package org.simplon.khizo.security.service;


import lombok.RequiredArgsConstructor;
import org.simplon.khizo.models.RoleEntity;
import org.simplon.khizo.models.RoleName;
import org.simplon.khizo.models.UserEntity;
import org.simplon.khizo.repositories.RoleEntityRepository;
import org.simplon.khizo.repositories.UserEntityRepository;
import org.simplon.khizo.security.config.JwtUtilities;
import org.simplon.khizo.security.dto.BearerToken;
import org.simplon.khizo.security.dto.LoginDto;
import org.simplon.khizo.security.dto.RegisterDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AppUserSecurityService implements IAppUserSecurityService {

    private final AuthenticationManager authenticationManager;
    private final UserEntityRepository userDao;

    private final RoleEntityRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtilities jwtUtilities;

    private static final String TOKEN_TYPE = "Bearer";

    @Override
    public ResponseEntity<?> register(RegisterDto rRegisterDto, RoleName roleName) {

        if (userDao.existsByUsername(rRegisterDto.getUsername())) {
            return new ResponseEntity<>("Username deja pris !", HttpStatus.BAD_REQUEST);
        }


        if (userDao.existsByEmail(rRegisterDto.getEmail())) {
            return new ResponseEntity<>("Email deja pris !", HttpStatus.BAD_REQUEST);
        }


        UserEntity user = new UserEntity();
        user.setUsername(rRegisterDto.getUsername());
        user.setEmail(rRegisterDto.getEmail());
        user.setPassword(passwordEncoder.encode(rRegisterDto.getPassword()));


        RoleEntity roleEntity = roleRepository.findByName(roleName);
        user.setRoles(Collections.singleton(roleEntity));


        userDao.save(user);


        if (roleName.equals(RoleName.USER)) {
            String token = jwtUtilities.generateToken(rRegisterDto.getUsername(),
                    Collections.singletonList(roleEntity.getRoleName()));
            return new ResponseEntity<>(new BearerToken(token, TOKEN_TYPE), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public BearerToken authenticate(LoginDto rLoginDto) {
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                rLoginDto.getUsernameOrEmail(), rLoginDto.getPassword());
        Authentication authentication = authenticationManager.authenticate(authToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserEntity user = userDao.findByEmail(authentication.getName())
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));

        List<String> rolesNames = new ArrayList<>();
        user.getRoles().forEach(r -> rolesNames.add(r.getRoleName()));
        return new BearerToken(jwtUtilities.generateToken(user.getUsername(), rolesNames), TOKEN_TYPE);
    }

}
