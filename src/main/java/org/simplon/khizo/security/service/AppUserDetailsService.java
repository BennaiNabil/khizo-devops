package org.simplon.khizo.security.service;

import lombok.RequiredArgsConstructor;
import org.simplon.khizo.models.UserEntity;
import org.simplon.khizo.repositories.UserEntityRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AppUserDetailsService implements UserDetailsService {

    private final UserEntityRepository UserRepository;

    @Override
    public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
        UserEntity user = UserRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)
                .orElseThrow(() ->
                        new UsernameNotFoundException("Utilisateur inconnu : " + usernameOrEmail));

        Set<GrantedAuthority> authorities = user.getRoles().stream()
                .map((role) -> new SimpleGrantedAuthority(role.getRoleName())).collect(Collectors.toSet());

        return new User(user.getEmail(), user.getPassword(), authorities);
    }

    public UserEntity loadUserById(Long id) {
        return UserRepository.findById(id).orElseThrow(
                () -> new UsernameNotFoundException("Utilisateur non trouvé avec l'id : " + id)
        );
    }

}