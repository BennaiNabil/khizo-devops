package org.simplon.khizo;

import org.simplon.khizo.models.*;
import org.simplon.khizo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


@SpringBootApplication
public class KhizoApplication {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserEntityRepository userEntityRepository;
    @Autowired
    private StepEntityRepository stepEntityRepository;
    @Autowired
    private IngredientEntityRepository ingredientEntityRepository;
    @Autowired
    private RecipeEntityRepository recipeEntityRepository;
    @Autowired
    private RoleEntityRepository roleEntityRepository;


    public static void main(final String[] args) {
        SpringApplication.run(KhizoApplication.class, args);
    }


//    @Profile("dev")
//    @Bean
//    public ApplicationRunner devApplicationRunner() {
//        return arg -> {
//
//            RoleEntity roleUser = new RoleEntity();
//            roleUser.setName(RoleName.USER);
//            RoleEntity roleAdmin = new RoleEntity();
//            roleAdmin.setName(RoleName.ADMIN);
//            roleUser = roleEntityRepository.save(roleUser);
//            roleAdmin = roleEntityRepository.save(roleAdmin);
//
//
//            UserEntity admin = new UserEntity();
//            admin.setUsername("admin");
//            admin.setEmail("admin@gmail.com");
//            admin.setPassword(passwordEncoder.encode("admin"));
//            admin.setRoles(new HashSet<>(List.of(roleAdmin, roleUser)));
//            admin = userEntityRepository.save(admin);
//
//            UserEntity user = new UserEntity();
//            user.setUsername("user");
//            user.setEmail("user@gmail.com");
//            user.setPassword(passwordEncoder.encode("user"));
//            user.setRoles(new HashSet<>(List.of(roleUser)));
//            user = userEntityRepository.save(user);
//
////            saveCarbonara(user);
////            saveChickenSandwich(user);
////            saveSalad(user);
//        };
//    }

    private void saveCarbonara(UserEntity user) {
        RecipeEntity carbonara = new RecipeEntity();
        carbonara.setTitle("Spaghetti Carbonara");
        carbonara.setCreatedAt(LocalDateTime.now());
        carbonara.setDescription("A classic Italian pasta dish.");
        carbonara.setIsPrivate(false);
        carbonara.setCreator(user);
        carbonara = recipeEntityRepository.save(carbonara);

        List<IngredientEntity> ingredients = new ArrayList<>();
        IngredientEntity spaghetti = new IngredientEntity();
        spaghetti.setUnit("grams");
        spaghetti.setName("Spaghetti");
        spaghetti.setQuantity(200.0);
        spaghetti.setRecipeEntity(carbonara);
        spaghetti = ingredientEntityRepository.save(spaghetti);

        IngredientEntity eggs = new IngredientEntity();
        eggs.setUnit("pieces");
        eggs.setName("Eggs");
        eggs.setQuantity(2.0);
        eggs.setRecipeEntity(carbonara);
        eggs = ingredientEntityRepository.save(eggs);

        IngredientEntity parmesanCheese = new IngredientEntity();
        parmesanCheese.setUnit("grams");
        parmesanCheese.setName("Parmesan Cheese");
        parmesanCheese.setQuantity(50.0);
        parmesanCheese.setRecipeEntity(carbonara);
        parmesanCheese = ingredientEntityRepository.save(parmesanCheese);

        IngredientEntity blackPepper = new IngredientEntity();
        blackPepper.setUnit("teaspoons");
        blackPepper.setName("Black Pepper");
        blackPepper.setQuantity(1.0);
        blackPepper.setRecipeEntity(carbonara);
        blackPepper = ingredientEntityRepository.save(blackPepper);

        ingredients.add(spaghetti);
        ingredients.add(eggs);
        ingredients.add(parmesanCheese);
        ingredients.add(blackPepper);

        carbonara.setIngredients(ingredients);
        carbonara = recipeEntityRepository.save(carbonara);

        List<StepEntity> steps = new ArrayList<>();
        StepEntity step1 = new StepEntity();
        step1.setStepOrder(1);
        step1.setContent("Boil the spaghetti until al dente.");
        step1.setRecipe(carbonara);
        step1 = stepEntityRepository.save(step1);

        StepEntity step2 = new StepEntity();
        step2.setStepOrder(2);
        step2.setContent("Whisk the eggs and mix them with the cooked spaghetti.");
        step2.setRecipe(carbonara);
        step2 = stepEntityRepository.save(step2);

        StepEntity step3 = new StepEntity();
        step3.setStepOrder(3);
        step3.setContent("Add Parmesan cheese and black pepper. Toss well. Serve hot.");
        step3.setRecipe(carbonara);
        step3 = stepEntityRepository.save(step3);

        steps.add(step1);
        steps.add(step2);
        steps.add(step3);

        carbonara.setSteps(steps);

        recipeEntityRepository.save(carbonara);
    }

    private void saveChickenSandwich(UserEntity user) {
        RecipeEntity sandwich = new RecipeEntity();
        sandwich.setTitle("Chicken Sandwich");
        sandwich.setCreatedAt(LocalDateTime.now());
        sandwich.setDescription("A quick and delicious chicken sandwich.");
        sandwich.setIsPrivate(false);
        sandwich.setCreator(user);
        sandwich = recipeEntityRepository.save(sandwich);

        List<IngredientEntity> ingredients = new ArrayList<>();
        IngredientEntity chickenBreast = new IngredientEntity();
        chickenBreast.setUnit("pieces");
        chickenBreast.setName("Chicken Breast");
        chickenBreast.setQuantity(1.0);
        chickenBreast.setRecipeEntity(sandwich);
        chickenBreast = ingredientEntityRepository.save(chickenBreast);

        IngredientEntity bun = new IngredientEntity();
        bun.setUnit("pieces");
        bun.setName("Bun");
        bun.setQuantity(2.0);
        bun.setRecipeEntity(sandwich);
        bun = ingredientEntityRepository.save(bun);

        IngredientEntity lettuce = new IngredientEntity();
        lettuce.setUnit("leaves");
        lettuce.setName("Lettuce");
        lettuce.setQuantity(2.0);
        lettuce.setRecipeEntity(sandwich);
        lettuce = ingredientEntityRepository.save(lettuce);

        IngredientEntity tomato = new IngredientEntity();
        tomato.setUnit("slices");
        tomato.setName("Tomato");
        tomato.setQuantity(2.0);
        tomato.setRecipeEntity(sandwich);
        tomato = ingredientEntityRepository.save(tomato);


        ingredients.add(chickenBreast);
        ingredients.add(bun);
        ingredients.add(lettuce);
        ingredients.add(tomato);


        sandwich.setIngredients(ingredients);
        sandwich = recipeEntityRepository.save(sandwich);

        // Steps
        List<StepEntity> steps = new ArrayList<>();
        StepEntity step1 = new StepEntity();
        step1.setStepOrder(1);
        step1.setContent("Grill or cook the chicken breast until fully cooked.");
        step1.setDescription("Cooking");
        step1 = stepEntityRepository.save(step1);

        StepEntity step2 = new StepEntity();
        step2.setStepOrder(2);
        step2.setContent("Assemble the sandwich by placing lettuce, tomato, and cooked chicken breast between the buns.");
        step2.setDescription("Assembling");
        step2 = stepEntityRepository.save(step2);

        steps.add(step1);
        steps.add(step2);


        sandwich.setSteps(steps);
        sandwich = recipeEntityRepository.save(sandwich);
    }

    private void saveSalad(UserEntity user) {
        RecipeEntity salad = new RecipeEntity();
        salad.setTitle("Tomato Salad");
        salad.setCreatedAt(LocalDateTime.now());
        salad.setDescription("A refreshing and healthy tomato salad.");
        salad.setIsPrivate(false);
        salad.setCreator(user);
        salad = recipeEntityRepository.save(salad);

        // Ingredients
        List<IngredientEntity> ingredients = new ArrayList<>();
        IngredientEntity tomatoes = new IngredientEntity();
        tomatoes.setUnit("pieces");
        tomatoes.setName("Tomatoes");
        tomatoes.setQuantity(4.0);
        tomatoes.setRecipeEntity(salad);
        tomatoes = ingredientEntityRepository.save(tomatoes);

        IngredientEntity cucumber = new IngredientEntity();
        cucumber.setUnit("pieces");
        cucumber.setName("Cucumber");
        cucumber.setQuantity(1.0);
        cucumber.setRecipeEntity(salad);
        cucumber = ingredientEntityRepository.save(cucumber);

        IngredientEntity redOnion = new IngredientEntity();
        redOnion.setUnit("pieces");
        redOnion.setName("Red Onion");
        redOnion.setQuantity(1.0);
        redOnion.setRecipeEntity(salad);
        redOnion = ingredientEntityRepository.save(redOnion);

        IngredientEntity oliveOil = new IngredientEntity();
        oliveOil.setUnit("tablespoons");
        oliveOil.setName("Olive Oil");
        oliveOil.setQuantity(2.0);
        oliveOil.setRecipeEntity(salad);
        oliveOil = ingredientEntityRepository.save(oliveOil);

        IngredientEntity lemonJuice = new IngredientEntity();
        lemonJuice.setUnit("tablespoons");
        lemonJuice.setName("Lemon Juice");
        lemonJuice.setQuantity(1.0);
        lemonJuice.setRecipeEntity(salad);
        lemonJuice = ingredientEntityRepository.save(lemonJuice);

        ingredients.add(tomatoes);
        ingredients.add(cucumber);
        ingredients.add(redOnion);
        ingredients.add(oliveOil);
        ingredients.add(lemonJuice);

        salad.setIngredients(ingredients);
        salad = recipeEntityRepository.save(salad);

        // Steps
        List<StepEntity> steps = new ArrayList<>();
        StepEntity step1 = new StepEntity();
        step1.setStepOrder(1);
        step1.setContent("Wash and dice the tomatoes, cucumber, and red onion.");
        step1.setDescription("Preparation");
        step1 = stepEntityRepository.save(step1);

        StepEntity step2 = new StepEntity();
        step2.setStepOrder(2);
        step2.setContent("In a large bowl, combine the diced vegetables.");
        step2.setDescription("Mixing");
        step2 = stepEntityRepository.save(step2);

        StepEntity step3 = new StepEntity();
        step3.setStepOrder(3);
        step3.setContent("Drizzle olive oil and lemon juice over the salad. Toss to coat evenly.");
        step3.setDescription("Dressing");
        step3 = stepEntityRepository.save(step3);


        steps.add(step1);
        steps.add(step2);
        steps.add(step3);

        salad.setSteps(steps);
        salad = recipeEntityRepository.save(salad);

    }

}
