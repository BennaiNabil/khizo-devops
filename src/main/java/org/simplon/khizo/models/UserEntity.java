package org.simplon.khizo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"username"}),
        @UniqueConstraint(columnNames = {"email"})
})
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iduser")
    private Long idUser;

    private String username;

    private String email;

    @JsonIgnore
    private String password;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "iduser", referencedColumnName = "iduser"),
            inverseJoinColumns = @JoinColumn(name = "idrole", referencedColumnName = "idrole"))
    private Set<RoleEntity> roles;

    private LocalDateTime bannedAt;

    private LocalDateTime bannedUntil;

    // A user can have many friends
    @ManyToMany
    @JoinTable(name = "user_friends",
            joinColumns = @JoinColumn(name = "iduser", referencedColumnName = "iduser"),
            inverseJoinColumns = @JoinColumn(name = "idfriend", referencedColumnName = "iduser"))
    private List<UserEntity> friends;

    // A user can have many notifications
    @OneToMany(mappedBy = "user")
    @JsonIgnore
    private List<NotificationEntity> notifications;

    // A user can have many recipes
    @OneToMany(mappedBy = "creator")
    @JsonIgnore
    private List<RecipeEntity> recipes;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_favorites",
            joinColumns = @JoinColumn(name = "iduser", referencedColumnName = "iduser"),
            inverseJoinColumns = @JoinColumn(name = "idrecipe", referencedColumnName = "idrecipe"))
    private List<RecipeEntity> favorites;

    @ManyToMany(mappedBy = "friends")
    private Collection<UserEntity> friendedBy;


}