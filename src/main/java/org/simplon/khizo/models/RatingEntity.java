package org.simplon.khizo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "ratings")
public class RatingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idrating", nullable = false)
    private Long idRating;

    @Column(name = "ratingvalue", nullable = false)
    private Integer ratingValue;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "idrecipe")
    private RecipeEntity idrecipe;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "iduser")
    private UserEntity iduser;


    public RatingEntity(Integer i) {
        this.ratingValue = i;
    }

    public RatingEntity() {
    }


}