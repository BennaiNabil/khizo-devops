package org.simplon.khizo.models.dto;

import lombok.Data;
import org.simplon.khizo.models.RecipeEntity;

import java.util.List;

/**
 * This DTO object contains 3 different pieces of data:
 * <ul>
 * <li>The total number of recipes</li>
 * <li>The last 6 visited recipes</li>
 * <li>The last 3 recipes created</li>
 * </ul>
 */
@Data
public class HomepageDataDto {
    private Integer nbRecipes;
    private List<RecipeEntity> lastSixVisitedRecipes;
    private List<RecipeEntity> lastThreeCreatedRecipes;
}
