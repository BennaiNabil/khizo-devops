package org.simplon.khizo.models.dto;

import lombok.Data;

@Data
public class RatingDto {
    private Long idrecipe;
    private Integer value;
}
