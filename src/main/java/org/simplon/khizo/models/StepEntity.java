package org.simplon.khizo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "steps")
public class StepEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idstep", nullable = false)
    private Long idStep;

    private Integer stepOrder;

    private String description;

    private String content;

    @ManyToOne
    @JoinColumn(name = "idrecipe", referencedColumnName = "idrecipe")
    @JsonIgnore
    private RecipeEntity recipe;

}