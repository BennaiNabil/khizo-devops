package org.simplon.khizo.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "roles")
public class RoleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idrole")
    private long idRole;

    @Enumerated(EnumType.STRING)
    @Column(name = "rolename")
    private RoleName name;

    public RoleEntity(RoleName roleName) {
        this.name = roleName;
    }

    public String getRoleName() {
        return name.toString();
    }


}