package org.simplon.khizo.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "parameters")
public class Parameter {
    @Id
    @Size(max = 255)
    @Column(name = "parameterkey", nullable = false)
    private String parameterKey;

    @NotNull
    @Column(name = "parametervalue", nullable = false)
    private Integer parameterValue;

}