package org.simplon.khizo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@Entity
@Table(name = "recipes")
public class RecipeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idrecipe", nullable = false)
    private Long idRecipe;

    private String title;

    private String description;

    private Boolean isPrivate;

    @CreationTimestamp
    private LocalDateTime createdAt;

    private LocalDateTime lastViewedAt;

    @ManyToOne(optional = false)
    @JoinColumn(name = "idcreator", nullable = false)
    @JsonIgnore
    private UserEntity creator;

    @OneToMany(mappedBy = "recipe")
    @Cascade(CascadeType.ALL)
    private List<StepEntity> steps;

    @OneToMany(mappedBy = "recipeEntity", orphanRemoval = true)
    @Cascade(CascadeType.ALL)
    private List<IngredientEntity> ingredients = new ArrayList<>();

    @Transient
    private Double averageRating;

    @JsonIgnore
    @OneToMany(mappedBy = "idrecipe", cascade = jakarta.persistence.CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<RatingEntity> ratings = new ArrayList<>();

}