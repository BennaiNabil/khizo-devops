package org.simplon.khizo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

@Getter
@Setter
@Entity
@Table(name = "ingredients")
public class IngredientEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idingredient", nullable = false)
    private Long idIngredient;

    private String name;

    private Double quantity;

    private String unit;

    @ManyToOne
    @JoinColumn(name = "idrecipe")
    @JsonIgnore
    private RecipeEntity recipeEntity;

}