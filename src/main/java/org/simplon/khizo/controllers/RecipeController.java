package org.simplon.khizo.controllers;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.simplon.khizo.models.RecipeEntity;
import org.simplon.khizo.models.dto.HomepageDataDto;
import org.simplon.khizo.models.dto.RatingDto;
import org.simplon.khizo.services.RatingService;
import org.simplon.khizo.services.RecipeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/recipes")
@Tag(name = "Recipe Controller", description = "API pour les opérations CRUD sur les recettes")
@RequiredArgsConstructor
public class RecipeController {

    private final RecipeService recipeService;
    private final RatingService ratingService;

    /**
     * Get all recipes of the user
     *
     * @param authentication the authentication object of the user
     * @return the list of recipes of the user
     */
    @GetMapping
    public ResponseEntity<List<RecipeEntity>> getAllRecipesOfUser(Authentication authentication) {
        String authenticationName = authentication.getName();
        List<RecipeEntity> recipes = recipeService.getRecipesAvailable(authenticationName);
        return ResponseEntity.ok().body(recipes);
    }

    /**
     * Get a recipe by its ID
     *
     * @param recipeId       the ID of the recipe
     * @param authentication the authentication object of the user
     * @return the recipe if the user is the owner, 403 otherwise
     */
    @GetMapping("/{recipeId}")
    public ResponseEntity<RecipeEntity> getRecipeById(@PathVariable Long recipeId,
                                                      Authentication authentication) {
        RecipeEntity recipe = recipeService.getRecipeById(recipeId);
        String userAuthenticator = authentication.getName();
        boolean isRecipeOwner = recipeService.checkOwner(recipe, userAuthenticator, userAuthenticator);
        if (isRecipeOwner) {
            return ResponseEntity.ok().body(recipe);
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    /**
     * Update a recipe
     *
     * @param recipeId       the ID of the recipe
     * @param recipeToUpdate the recipe to update
     * @param authentication the authentication object of the user
     * @return the updated recipe if the user is the owner, 403 otherwise
     */
    @PutMapping("/{recipeId}")
    public ResponseEntity<RecipeEntity> updateRecipe(@PathVariable Long recipeId, @RequestBody RecipeEntity recipeToUpdate,
                                                     Authentication authentication) {
        RecipeEntity existingRecipe = recipeService.getRecipeByIdMute(recipeId);
        String userAuthenticator = authentication.getName();
        boolean isRecipeOwner = recipeService.checkOwner(existingRecipe, userAuthenticator, userAuthenticator);
        if (isRecipeOwner) {
            RecipeEntity recipe = recipeService.updateRecipe(recipeId, recipeToUpdate, userAuthenticator);
            return ResponseEntity.ok().body(recipe);
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    /**
     * Delete a recipe by its ID
     *
     * @param recipeId       the ID of the recipe
     * @param authentication the authentication object of the user
     * @return 204 if the recipe has been deleted successfully, 403 otherwise
     */
    @DeleteMapping("/{recipeId}")
    public ResponseEntity<Void> deleteRecipe(@PathVariable Long recipeId,
                                             Authentication authentication) {
        RecipeEntity recipe = recipeService.getRecipeByIdMute(recipeId);
        String userAuthenticator = authentication.getName();
        boolean isRecipeOwner = recipeService.checkOwner(recipe, userAuthenticator, userAuthenticator);
        if (isRecipeOwner) {
            recipeService.deleteRecipe(recipeId);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    /**
     * Create a recipe
     *
     * @param recipe         the recipe to create
     * @param authentication the authentication object of the user
     * @return the created recipe
     */
    @PostMapping
    public ResponseEntity<RecipeEntity> createRecipe(@RequestBody RecipeEntity recipe, Authentication authentication) {
        RecipeEntity createdRecipe = recipeService.createRecipe(recipe, authentication.getName());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdRecipe);
    }

    /**
     * Get the homepage data of the user
     *
     * @param authentication the authentication object of the user
     * @return the homepage data of the user
     */
    @GetMapping("/homedata")
    public ResponseEntity<HomepageDataDto> getHomepageData(Authentication authentication) {
        String authenticationName = authentication.getName();
        HomepageDataDto homepageData = recipeService.getHomepageData(authenticationName);
        return ResponseEntity.ok().body(homepageData);
    }


    @PostMapping("/rate")
    public ResponseEntity<Void> rateRecipe(@RequestBody RatingDto ratingDto, Authentication authentication) {
        String authenticationName = authentication.getName();
        ratingService.rateRecipe(ratingDto, authenticationName);
        return ResponseEntity.noContent().build();
    }


    @PostMapping("/favorite")
    public ResponseEntity<Void> favoriteRecipe(@RequestBody Long recipeId, Authentication authentication) {
        String authenticationName = authentication.getName();
        RecipeEntity recipe = recipeService.getRecipeByIdMute(recipeId);
        boolean isRecipeOwner = recipeService.checkOwner(recipe, authenticationName, authenticationName);
        if (isRecipeOwner) {
            recipeService.favoriteRecipe(recipeId, authenticationName);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PostMapping("/unfavorite")
    public ResponseEntity<Void> unfavoriteRecipe(@RequestBody Long recipeId, Authentication authentication) {
        String authenticationName = authentication.getName();
        RecipeEntity recipe = recipeService.getRecipeByIdMute(recipeId);
        boolean isRecipeOwner = recipeService.checkOwner(recipe, authenticationName, authenticationName);
        boolean isRecipeFavorited = recipeService.isRecipeFavorited(recipeId, authenticationName);
        if (isRecipeOwner) {
            if (!isRecipeFavorited) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            } else {
                recipeService.unfavoriteRecipe(recipeId, authenticationName);
                return ResponseEntity.noContent().build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }


}
