package org.simplon.khizo.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EntityScan("org.simplon.khizo.models")
@EnableJpaRepositories("org.simplon.khizo.repositories")
@EnableTransactionManagement
public class DomainConfig {
}
