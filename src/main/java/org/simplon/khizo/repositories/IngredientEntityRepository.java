package org.simplon.khizo.repositories;

import org.simplon.khizo.models.IngredientEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IngredientEntityRepository extends JpaRepository<IngredientEntity, Long> {
}