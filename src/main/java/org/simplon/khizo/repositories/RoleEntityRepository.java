package org.simplon.khizo.repositories;

import org.simplon.khizo.models.RoleEntity;
import org.simplon.khizo.models.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RoleEntityRepository extends JpaRepository<RoleEntity, Long> {
    RoleEntity findByName(RoleName roleName);
}