package org.simplon.khizo.repositories;

import org.simplon.khizo.models.RatingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface RatingEntityRepository extends JpaRepository<RatingEntity, Long> {
    @Query("select r from RatingEntity r where r.idrecipe.idRecipe = ?1 and r.iduser.email = ?2")
    Optional<RatingEntity> findPreviousRating(Long idRecipe, String email);

    @Query("select (count(r) > 0) from RatingEntity r where r.idrecipe.creator.email = ?1 and r.idrecipe.idRecipe = ?2")
    boolean hasAlreadyRatedRecipe(String email, Long idRecipe);
}