package org.simplon.khizo.repositories;

import org.simplon.khizo.models.RecipeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface RecipeEntityRepository extends JpaRepository<RecipeEntity, Long> {
    Optional<RecipeEntity> findByTitle(String title);
    long countByCreator_Email(String email);

    @Query("select r from RecipeEntity r where r.creator.username = ?1 or r.creator.email = ?1 or r.isPrivate = false")
    List<RecipeEntity> findAllByCreatorUsername(String authenticationName);

    @Query("""
            select r from RecipeEntity r
            where r.creator.username = ?1 or r.creator.email = ?1
            order by r.lastViewedAt DESC limit 6""")
    List<RecipeEntity> findTop6ByOrderByLastVisitedDesc(String authenticationName);

    @Query("""
            select r from RecipeEntity r
            where r.creator.username = ?1 or r.creator.email = ?1
            order by r.idRecipe DESC limit 3""")
    List<RecipeEntity> findTop3ByOrderByCreationDateDesc(String authenticationName);


}