package org.simplon.khizo.repositories;

import org.simplon.khizo.models.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserEntityRepository extends JpaRepository<UserEntity, Long> {
    boolean existsByUsername(String username);
    boolean existsByEmail(String email);
    Optional<UserEntity> findByEmail(String name);
    @Query("select u from UserEntity u where u.username = ?1 or u.email = ?2")
    Optional<UserEntity> findByUsernameOrEmail(String usernameOrEmail, String usernameOrEmail1);
}