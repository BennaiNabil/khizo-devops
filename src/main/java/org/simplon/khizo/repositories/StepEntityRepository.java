package org.simplon.khizo.repositories;

import org.simplon.khizo.models.StepEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StepEntityRepository extends JpaRepository<StepEntity, Long> {
    List<StepEntity> findByRecipe_IdRecipe(Long idRecipe);
}