package org.simplon.khizo.services;

import org.simplon.khizo.models.RatingEntity;
import org.simplon.khizo.models.RecipeEntity;
import org.simplon.khizo.models.UserEntity;
import org.simplon.khizo.models.dto.RatingDto;
import org.simplon.khizo.repositories.RatingEntityRepository;
import org.simplon.khizo.repositories.RecipeEntityRepository;
import org.simplon.khizo.repositories.UserEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingService {

    @Autowired
    private RatingEntityRepository ratingRepository;
    @Autowired
    private RecipeEntityRepository recipeEntityRepository;
    @Autowired
    private UserEntityRepository userEntityRepository;

    public void rateRecipe(RatingDto ratingDto, String authenticationName) {
        //  Check if the rating value is valid (between 0 and 5)
        if (Math.abs(2.5 - ratingDto.getValue()) > 2.5) {
            throw new RuntimeException("Invalid rating value");
        }

        //  Check if the user has already rated the recipe
        boolean hasAlreadyRatedRecipe = ratingRepository.hasAlreadyRatedRecipe(authenticationName, ratingDto.getIdrecipe());
        if (hasAlreadyRatedRecipe) {
            RatingEntity ratingEntity = ratingRepository.findPreviousRating(ratingDto.getIdrecipe(), authenticationName)
                    .orElseThrow(() -> new RuntimeException("Rating not found"));
            ratingEntity.setRatingValue(ratingDto.getValue());
            ratingRepository.save(ratingEntity);
            return;
        }
        //  If not, create a new rating
        RatingEntity ratingEntity = new RatingEntity();
        RecipeEntity recipe = recipeEntityRepository.findById(ratingDto.getIdrecipe())
                .orElseThrow(() -> new RuntimeException("Recipe not found"));
        UserEntity user = userEntityRepository.findByUsernameOrEmail(authenticationName, authenticationName)
                .orElseThrow(() -> new RuntimeException("User not found"));
        ratingEntity.setIdrecipe(recipe);
        ratingEntity.setIduser(user);
        ratingEntity.setRatingValue(ratingDto.getValue());
        ratingRepository.save(ratingEntity);
    }
}
