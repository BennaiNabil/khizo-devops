package org.simplon.khizo.services;

import lombok.extern.slf4j.Slf4j;
import org.simplon.khizo.models.RatingEntity;
import org.simplon.khizo.models.RecipeEntity;
import org.simplon.khizo.models.UserEntity;
import org.simplon.khizo.models.dto.HomepageDataDto;
import org.simplon.khizo.repositories.RecipeEntityRepository;
import org.simplon.khizo.repositories.UserEntityRepository;
import org.simplon.khizo.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.OptionalDouble;

@Service
@Slf4j
public class RecipeService {
    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private RecipeEntityRepository recipeDao;

    public Double calculateAverageRating(List<RatingEntity> ratings) {
        OptionalDouble average = ratings.stream().mapToDouble(RatingEntity::getRatingValue).average();
        if (average.isPresent()) {
            return Math.round(average.getAsDouble() * 100.0) / 100.0;
        }
        return null;
    }

    public RecipeEntity createRecipe(RecipeEntity recipe, String name) {
        userEntityRepository.findByUsernameOrEmail(name, name).ifPresent(recipe::setCreator);
        recipe.setAverageRating(calculateAverageRating(recipe.getRatings()));
        return recipeDao.save(recipe);
    }

    public RecipeEntity updateRecipe(Long id, RecipeEntity updatedRecipe, String userAuthenticator) {
        if (!recipeDao.existsById(id)) {
            throw new NotFoundException("RecipeEntity not found with ID: " + id);
        }
        UserEntity user = userEntityRepository.findByUsernameOrEmail(userAuthenticator, userAuthenticator)
                .orElseThrow(() -> new NotFoundException("UserEntity not found with username or email: " + userAuthenticator));
        updatedRecipe.setCreator(user);
        updatedRecipe.setIdRecipe(id);
        updatedRecipe.setAverageRating(calculateAverageRating(updatedRecipe.getRatings()));
        updatedRecipe.getSteps().forEach(stepEntity -> stepEntity.setRecipe(updatedRecipe));
        return recipeDao.save(updatedRecipe);
    }

    public void deleteRecipe(Long id) {
        if (!recipeDao.existsById(id)) {
            throw new NotFoundException("RecipeEntity not found with ID: " + id);
        }
        recipeDao.deleteById(id);
    }

    public List<RecipeEntity> getRecipesAvailable(String authenticationName) {
        return recipeDao.findAllByCreatorUsername(authenticationName).stream()
                .peek(recipe -> recipe.setAverageRating(calculateAverageRating(recipe.getRatings())))
                .toList();
    }

    public RecipeEntity getRecipeById(Long recipeId) {
        RecipeEntity recipeEntity = recipeDao.findById(recipeId).
                orElseThrow(() -> new NotFoundException("RecipeEntity not found with ID: " + recipeId));
        List<RatingEntity> ratings = recipeEntity.getRatings();
        recipeEntity.setAverageRating(calculateAverageRating(ratings));
        return recordVisit(recipeEntity);
    }

    public RecipeEntity getRecipeByIdMute(Long recipeId) {
        RecipeEntity recipeEntity = recipeDao.findById(recipeId).
                orElseThrow(() -> new NotFoundException("RecipeEntity not found with ID: " + recipeId));
        recipeEntity.setAverageRating(calculateAverageRating(recipeEntity.getRatings()));
        return recipeEntity;
    }

    public HomepageDataDto getHomepageData(String authenticationName) {
        HomepageDataDto homepageDataDto = new HomepageDataDto();
        homepageDataDto.setNbRecipes(recipeDao.findAllByCreatorUsername(authenticationName).size());
        homepageDataDto.setLastSixVisitedRecipes(recipeDao.findTop6ByOrderByLastVisitedDesc(authenticationName));
        homepageDataDto.setLastThreeCreatedRecipes(recipeDao.findTop3ByOrderByCreationDateDesc(authenticationName));
        return homepageDataDto;
    }

    public RecipeEntity recordVisit(RecipeEntity recipe) {
        recipe.setLastViewedAt(LocalDateTime.now());
        return recipeDao.save(recipe);
    }

    public void favoriteRecipe(Long recipeId, String authenticationName) {
        RecipeEntity recipe = recipeDao.findById(recipeId)
                .orElseThrow(() -> new NotFoundException("RecipeEntity not found with ID: " + recipeId));
        UserEntity user = userEntityRepository.findByUsernameOrEmail(authenticationName, authenticationName)
                .orElseThrow(() -> new NotFoundException("UserEntity not found with username or email: " + authenticationName));
        user.getFavorites().add(recipe);
        userEntityRepository.save(user);
    }

    public boolean isRecipeFavorited(Long recipeId, String authenticationName) {
        RecipeEntity recipe = recipeDao.findById(recipeId)
                .orElseThrow(() -> new NotFoundException("RecipeEntity not found with ID: " + recipeId));
        UserEntity user = userEntityRepository.findByUsernameOrEmail(authenticationName, authenticationName)
                .orElseThrow(() -> new NotFoundException("UserEntity not found with username or email: " + authenticationName));
        return user.getFavorites().contains(recipe);
    }

    public void unfavoriteRecipe(Long recipeId, String authenticationName) {
        RecipeEntity recipe = recipeDao.findById(recipeId)
                .orElseThrow(() -> new NotFoundException("RecipeEntity not found with ID: " + recipeId));
        UserEntity user = userEntityRepository.findByUsernameOrEmail(authenticationName, authenticationName)
                .orElseThrow(() -> new NotFoundException("UserEntity not found with username or email: " + authenticationName));
        user.getFavorites().remove(recipe);
        userEntityRepository.save(user);
    }

    /**
     * Checks if the user is the owner of the recipe
     *
     * @param recipeEntity the recipe to check
     * @param username     the username of the user
     * @param email        the email of the user
     * @return true if the user is the owner of the recipe, false otherwise
     */
    public boolean checkOwner(RecipeEntity recipeEntity, String username, String email) {
        return recipeEntity.getCreator().getUsername().equals(username) || recipeEntity.getCreator().getEmail().equals(email);
    }
}
