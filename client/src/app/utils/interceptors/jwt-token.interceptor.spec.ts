import {TestBed} from '@angular/core/testing';

import {JwtTokenInterceptor} from './jwt-token.interceptor';
import {HttpHandler, HttpRequest} from "@angular/common/http";

describe('JwtTokenInterceptor', () => {
  let service: JwtTokenInterceptor;
  let httpMock: { handle: jasmine.Spy };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        JwtTokenInterceptor,
        {
          provide: HttpHandler,
          useValue: {
            handle: jasmine.createSpy('handle')
          }
        }
      ]
    });
    service = TestBed.inject(JwtTokenInterceptor);
    httpMock = TestBed.inject(HttpHandler) as unknown as { handle: jasmine.Spy };
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should add authorization header when there is a token', () => {
    const request = new HttpRequest('GET', 'http://localhost:4200');
    const token = 'sampletoken';
    spyOn(localStorage, 'getItem').and.returnValue(token);
    const expectedRequest = request.clone({setHeaders: {Authorization: `Bearer ${token}`}});

    service.intercept(request, httpMock as HttpHandler);

    expect(httpMock.handle).toHaveBeenCalledWith(expectedRequest);
  });

  it('should not add authorization header when there is no token', () => {
    const request = new HttpRequest('GET', 'http://localhost:4200');
    spyOn(localStorage, 'getItem').and.returnValue(null);

    service.intercept(request, httpMock as HttpHandler);

    expect(httpMock.handle).toHaveBeenCalledWith(request);
  });
});
