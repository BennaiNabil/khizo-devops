import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment.development";

@Injectable()
export class JwtTokenInterceptor implements HttpInterceptor {

  constructor() {
  }

  /**
   * Intercepts HTTP requests and adds authorization token to the request headers.
   *
   * @param {HttpRequest<unknown>} request - The HTTP request.
   * @param {HttpHandler} next - The HTTP handler.
   *
   **/
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token: string | null = localStorage.getItem(environment.tokenId);
    const bearer = 'Bearer ';
    const authorization = 'Authorization';

    let requestWithToken = request;

    if (token) {
      requestWithToken = request.clone({
        headers: request.headers.set(authorization, bearer + token)
      });
    }
    return next.handle(requestWithToken);
  }
}
