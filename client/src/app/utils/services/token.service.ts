import {Injectable} from '@angular/core';
import {environment} from "../../../environments/environment.development";

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() {
  }

  isLoggedIn() {
    const token = localStorage.getItem(environment.tokenId);
    return !!token;
  }

  logout() {
    localStorage.removeItem(environment.tokenId);
  }

}
