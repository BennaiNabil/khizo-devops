import {TokenService} from './token.service';
import {environment} from "../../../environments/environment.development";

describe('TokenService', () => {
  let service: TokenService;

  beforeEach(() => {
    service = new TokenService();
  });

  describe('#isLoggedIn', () => {
    it('should return true when the tokenId is present in localStorage', () => {
      spyOn(localStorage, 'getItem').and.returnValue('dummyTokenId');
      expect(service.isLoggedIn()).toBe(true);
    });
    it('should return false when the tokenId is not present in localStorage', () => {
      spyOn(localStorage, 'getItem').and.returnValue(null);
      expect(service.isLoggedIn()).toBe(false);
    });
  });

  describe('#logout', () => {
    it('should remove tokenId from localStorage on logout', () => {
      spyOn(localStorage, 'removeItem');
      service.logout();
      expect(localStorage.removeItem).toHaveBeenCalledWith(environment.tokenId);
    });
  });
});
