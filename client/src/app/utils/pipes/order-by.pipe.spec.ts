import {OrderByPipe} from './order-by.pipe';
import {TestBed} from '@angular/core/testing';

describe('OrderByPipe', () => {
  let pipe: OrderByPipe;

  beforeEach(() => {
    TestBed.configureTestingModule({providers: [OrderByPipe]});
    pipe = TestBed.inject(OrderByPipe);
  });

  it('should sort an array by provided field', () => {
    let array = [
      {price: 10},
      {price: 5},
      {price: 30}
    ];
    let result = pipe.transform(array, 'price');
    expect(result).toEqual([
      {price: 5},
      {price: 10},
      {price: 30}
    ]);
  });

});
