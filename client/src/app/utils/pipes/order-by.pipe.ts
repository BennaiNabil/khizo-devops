import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {
  transform(array: any[] | undefined, field: string): any[] {
    if (!array) {
      return [];
    }
    if (!Array.isArray(array)) {
      return array;
    }
    const compareValues = (a: any, b: any): number => a[field] < b[field] ? -1 :
      a[field] > b[field] ? 1 :
        0;
    array.sort(compareValues);
    return array;
  }
}
