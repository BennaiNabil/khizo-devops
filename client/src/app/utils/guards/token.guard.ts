import {environment} from "../../../environments/environment.development";

export const tokenGuard = () => {
  const token = localStorage.getItem(environment.tokenId);

  if (!token) {
    window.location.href = 'login';
  }

  return !!token;
};
