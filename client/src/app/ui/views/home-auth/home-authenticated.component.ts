import {Component} from '@angular/core';
import {HomepageDataDto, RecipeControllerService} from "../../../http";

@Component({
  selector: 'app-home-authenticated',
  templateUrl: './home-authenticated.component.html',
  styleUrls: ['./home-authenticated.component.css']
})
export class HomeAuthenticated {

  homepageData: HomepageDataDto = {} as HomepageDataDto;

  constructor(private recipeService: RecipeControllerService) {
    this.recipeService.getHomepageData().subscribe(data => {
      return this.homepageData = data;
    })
  }


}
