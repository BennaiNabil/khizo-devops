import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeAuthenticated } from './home-authenticated.component';

describe('FavouritesComponent', () => {
  let component: HomeAuthenticated;
  let fixture: ComponentFixture<HomeAuthenticated>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HomeAuthenticated]
    });
    fixture = TestBed.createComponent(HomeAuthenticated);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
