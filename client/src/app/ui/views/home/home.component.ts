import {AfterContentChecked, Component} from '@angular/core';
import {TokenService} from "../../../utils/services/token.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements AfterContentChecked {
  isAuth: boolean = false;

  constructor(private tokenService: TokenService) {
    this.handleAuthStatus();
  }

  private handleAuthStatus() {
    this.isAuth = this.tokenService.isLoggedIn();
  }

  ngAfterContentChecked(): void {
    this.handleAuthStatus();
  }



}
