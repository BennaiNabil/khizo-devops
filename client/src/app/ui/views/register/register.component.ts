import {HttpErrorResponse} from '@angular/common/http';
import {Component} from '@angular/core';
import {BearerToken, LoginControllerService} from 'src/app/http';
import {environment} from "../../../../environments/environment.development";
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  myForm: FormGroup;
  registrationFailed: boolean = false;

  constructor(private fb: FormBuilder, private authentificationService: LoginControllerService, private router: Router) {
    this.myForm = this.fb.group({
      username: [''],
      email: [''],
      password: ['']
    });
  }

  /**
   * Handles the submission of the form.
   *
   * @return {void}
   */
  onSubmit(): void {
    const values = this.myForm.value;
    this.authentificationService.registerUser(values).subscribe(
      async (value: BearerToken) => await this.handleAccessTokenAndNavigate(value),
      (error: HttpErrorResponse) => this.handleRegistrationError(error));
  }

  /**
   * Handle registration error.
   *
   * @param {HttpErrorResponse} error - The error received from the API call.
   * @returns {void}
   */
  private handleRegistrationError(error: HttpErrorResponse): void {
    if (error.status !== 200) {
      this.registrationFailed = true;
    }
  }

  /**
   * Handles the bearer token and navigates to a specified route if the token is valid.
   *
   * @param value - The bearer token.
   * @returns {Promise<void>} - A promise that resolves once the token is handled and navigation is complete.
   */
  private async handleAccessTokenAndNavigate(value: BearerToken): Promise<void> {
    if (value.accessToken) {
      localStorage.setItem(environment.tokenId, value.accessToken);
      await this.router.navigate(['']);
    }
  }
}
