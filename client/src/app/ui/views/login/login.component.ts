import {Component} from '@angular/core';
import {BearerToken, LoginControllerService} from "../../../http";
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {environment} from "../../../../environments/environment.development";

@Component({
  selector: 'app-login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  myForm: FormGroup;
  authenticationFailed: boolean = false;

  private readonly homeRoute = '';

  constructor(private fb: FormBuilder,
              private authenticationService: LoginControllerService,
              private router: Router) {
    this.myForm = this.fb.group({
      usernameOrEmail: [''],
      password: ['']
    });
  }

  /**
   * Handles the submission of the login form by authenticating the user.
   *
   * @return {void}
   */
  onSubmit(): void {
    const loginData = this.myForm.value;
    this.authenticationService.authenticateUser(loginData).subscribe(
      (value: BearerToken) => this.handleAuthentication(value),
      (error: HttpErrorResponse) => this.handleLoginFail(error)
    );
  }

  /**
   * Handles authentication by saving the access token in the local storage and navigating to the home route.
   *
   * @param {BearerToken} value - The bearer token containing the access token.
   * @return {void}
   */
  private handleAuthentication(value: BearerToken): void {
    if (value.accessToken) {
      localStorage.setItem(environment.tokenId, value.accessToken);
      this.router.navigate([this.homeRoute]);
    }
  }

  /**
   * Handles the failed login attempt.
   *
   * @param {HttpErrorResponse} error - The error response object.
   */
  private handleLoginFail(error: HttpErrorResponse): void {
    if (error.status !== 200) {
      this.authenticationFailed = true;
    }
  }
}
