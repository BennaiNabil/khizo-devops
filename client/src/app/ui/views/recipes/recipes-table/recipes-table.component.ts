import {Component, Input} from '@angular/core';
import {RecipeControllerService, RecipeEntity} from "../../../../http";
import {catchError} from "rxjs/operators";
import {of} from "rxjs";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-recipes-table',
  templateUrl: './recipes-table.component.html',
  styleUrls: ['./recipes-table.component.css']
})
export class RecipesTableComponent {

  @Input() userRecipes: Array<RecipeEntity> = [];
  selectedRecipe: RecipeEntity = {} as RecipeEntity;

  constructor(private recipeService: RecipeControllerService, private toastService: ToastrService) {
  }

  sortTable(key: string, asc: string) {
    const isAscending = asc === 'asc';
    const fields = [
      {keys: ['title', 'description'], getValue: (a: any) => a[key].toLowerCase()},
      {keys: ['createdAt'], getValue: (a: any) => new Date(a[key])},
      {keys: ['isPrivate'], getValue: (a: any) => a[key]},
    ];

    for (let field of fields) {
      if (field.keys.includes(key)) {
        this.userRecipes.sort((a: any, b: any) => this.sortValues(a, b, field.getValue, isAscending));
        break;
      }
    }
  }

  sortValues(a: any, b: any, getValue: (arg0: any) => any, isAscending: boolean) {
    const aValue = getValue(a);
    const bValue = getValue(b);
    if (aValue > bValue) {
      return isAscending ? 1 : -1;
    }
    if (aValue < bValue) {
      return isAscending ? -1 : 1;
    }
    return 0;
  }

  showDetails(recipe: RecipeEntity) {
    this.selectedRecipe = recipe;
  }

  confirmDelete(recipe: RecipeEntity) {
    const agree = confirm("Do you really want to delete this recipe?");
    if (agree) {
      this.deleteRecipe(recipe);
    }
  }

  private deleteRecipe(recipe: RecipeEntity) {
    this.recipeService.deleteRecipe(recipe.idRecipe as number).pipe(
      catchError((err) => {
        this.toastService.error("Error when trying to delete this recipe")
        setTimeout(() => window.location.reload(), 2000)
        return of(null)
      })
    ).subscribe(() => {
      this.userRecipes = this.userRecipes.filter(r => r.idRecipe != recipe.idRecipe)
    })
  }
}
