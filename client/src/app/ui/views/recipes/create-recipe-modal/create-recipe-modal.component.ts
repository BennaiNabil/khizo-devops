import {Component, Input} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {RecipeControllerService, RecipeEntity} from "../../../../http";

@Component({
  selector: 'app-create-recipe-modal',
  templateUrl: './create-recipe-modal.component.html',
  styleUrls: ['./create-recipe-modal.component.css']
})
export class CreateRecipeModalComponent {

  recipeForm!: FormGroup;
  @Input() userRecipes: Array<RecipeEntity> = [];

  constructor(private recipeService: RecipeControllerService, private formBuilder: FormBuilder) {
    this.recipeForm = this.formBuilder.group({
      title: '',
      description: '',
      isPrivate: false
    });
  }

  onSubmit() {
    this.recipeService.createRecipe(this.recipeForm.value).subscribe((recipe: RecipeEntity) => {
      this.recipeService.getAllRecipesOfUser().subscribe((recipes: Array<RecipeEntity>) => {
        this.userRecipes = recipes;
        document.getElementById('closeModalCreate')?.click();
      });
    });
  }


}
