import {Component, Input} from '@angular/core';
import {RecipeControllerService, RecipeEntity, StepEntity} from "../../../../http";
import {catchError} from "rxjs/operators";
import {Observable, of} from "rxjs";
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-recipe-details-modal',
  templateUrl: './recipe-details-modal.component.html',
  styleUrls: ['./recipe-details-modal.component.css']
})
export class RecipeDetailsModalComponent {
  @Input() recipe: RecipeEntity = {} as RecipeEntity;
  isNewStepFormVisible: boolean = false;
  newStep: StepEntity = {} as StepEntity;

  constructor(private recipeService: RecipeControllerService, private toastr: ToastrService) {
  }

  deleteStep(step: StepEntity) {
    this.removeStepFromRecipe(step);
    this.makeApiCallWithErrorHandling('Step cannot be deleted due to an error from the server.');
  }

  private removeStepFromRecipe(step: StepEntity) {
    this.recipe.steps = this.recipe.steps?.filter(pStem => pStem.idStep != step.idStep)
    debugger;
    }

  private makeApiCallWithErrorHandling(errorMsg: string) {
    this.recipeService.updateRecipe(this.recipe, this.recipe.idRecipe as number)
      .pipe(catchError((error) => this.handleServerError(error, errorMsg)))
      .subscribe(updatedRecipe => this.recipe = updatedRecipe ?? this.recipe);
  }

  addStep() {
    this.recipe.steps?.push(this.newStep);
        this.isNewStepFormVisible = false;
    this.makeApiCallWithErrorHandling('Error while creating the step');
  }

  private handleServerError(error: any, errorMsg: string): Observable<null> {
    console.error(error);
    this.presentToast(errorMsg);
    this.closeDetailsModalAndReload();
    return of(null);
  }

  private closeDetailsModalAndReload() {
    document.getElementById("closeDetailsModal")?.click();
    setTimeout(() => window.location.reload(), 500);
  }

  private presentToast(message: string) {
    this.toastr.error(message);
  }

  updateStep(step: StepEntity) {

  }
}
