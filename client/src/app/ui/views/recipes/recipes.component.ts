import {Component} from '@angular/core';
import {RecipeControllerService, RecipeEntity} from "../../../http";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent {

  userRecipes: Array<RecipeEntity> = [];
  recipeForm!: FormGroup;

  constructor(private recipeService: RecipeControllerService, private formBuilder: FormBuilder) {
    this.recipeService.getAllRecipesOfUser().subscribe((recipes: Array<RecipeEntity>) => {
      this.userRecipes = recipes;
    });
    this.recipeForm = this.formBuilder.group({
      title: '',
      description: '',
      isPrivate: false
    });

  }




}
