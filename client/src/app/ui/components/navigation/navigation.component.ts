import {Component} from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from 'src/app/utils/services/token.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {
  /**
   * A boolean value that indicates whether the user is logged in or not.
   */
  isLogged: boolean = false;

  /**
   * Constructs a new instance of NavigationComponent.
   *
   * @param {TokenService} authenticationService - The service to handle user authentication.
   * @param {Router} router - The Angular router to navigate between components.
   */
  constructor(private authenticationService: TokenService, private router: Router) {
    this.getAuthStatus();
  }

  /**
   * Angular lifecycle hook that is called after the component's view has been fully initialized.
   * Here it is used to update the authentication status of the user.
   */
  ngAfterViewChecked(): void {
    this.getAuthStatus();
  }

  /**
   * Logs out the user by calling the logout method from the authentication service.
   * Then navigate to the root route ('').
   */
  async logout() {
    this.authenticationService.logout();
    await this.router.navigate(['']);
  }

  /**
   * Checks if the user is logged in by calling the isLoggedIn method from the authentication service.
   * The result is stored in the isLogged property.
   */
  private getAuthStatus() {
    this.isLogged = this.authenticationService.isLoggedIn();
  }

}
