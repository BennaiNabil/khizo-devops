export * from './loginController.service';
import {LoginControllerService} from './loginController.service';
import {RecipeControllerService} from './recipeController.service';

export * from './recipeController.service';

export const APIS = [LoginControllerService, RecipeControllerService];
