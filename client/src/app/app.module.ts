import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavigationComponent} from './ui/components/navigation/navigation.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {APIS} from "./http";
import {JwtTokenInterceptor} from "./utils/interceptors/jwt-token.interceptor";
import {LoginComponent} from './ui/views/login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RegisterComponent} from './ui/views/register/register.component';
import {HomeComponent} from './ui/views/home/home.component';
import {RecipesComponent} from './ui/views/recipes/recipes.component';
import {CreateRecipeModalComponent} from './ui/views/recipes/create-recipe-modal/create-recipe-modal.component';
import {RecipesTableComponent} from './ui/views/recipes/recipes-table/recipes-table.component';
import {RecipeDetailsModalComponent} from './ui/views/recipes/recipe-details-modal/recipe-details-modal.component';
import {ToastrModule} from "ngx-toastr";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {OrderByPipe} from './utils/pipes/order-by.pipe';
import { HomeAuthenticated } from './ui/views/home-auth/home-authenticated.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    RecipesComponent,
    CreateRecipeModalComponent,
    RecipesTableComponent,
    RecipeDetailsModalComponent,
    OrderByPipe,
    HomeAuthenticated,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    FormsModule,
    // required animations module

  ],
  providers: [
    ...APIS,
    {provide: HTTP_INTERCEPTORS, useClass: JwtTokenInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
