import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from "./ui/views/login/login.component";
import {RegisterComponent} from "./ui/views/register/register.component";
import {HomeComponent} from "./ui/views/home/home.component";
import {RecipesComponent} from "./ui/views/recipes/recipes.component";
import {tokenGuard} from "./utils/guards/token.guard";
import {HomeAuthenticated} from "./ui/views/home-auth/home-authenticated.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'recipes', component: RecipesComponent, canActivate: [tokenGuard]},
  {path: 'authenticated', component: HomeAuthenticated, canActivate: [tokenGuard]},
  {path:'**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
